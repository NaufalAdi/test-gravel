# PokeBase

## Description

PokeDB is a website where you can see information about individual Pokemon and add them to your collection. This project utilizes NextJS and Tailwind CSS frameworks and https://pokeapi.co/docs/v2 API. This project is created for an internship test.

## URL

- / : Displays all available Pokemon
- /details/{name} : Displays the details of each individual Pokemon
- /my-pokemon : Shows all Pokemon caught by the user

## How to Run

1. `npm install` to install dependencies
2. `npm run dev` to run the project

## Deployed on

https://naufal-pokebase.vercel.app/
