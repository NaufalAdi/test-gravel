export default function sitemap() {
    return [
      {
        url: 'https://naufal-pokebase.vercel.app/',
        lastModified: new Date(),
      },
      {
        url: 'https://naufal-pokebase.vercel.app/my-pokemon',
        lastModified: new Date(),
      },
      {
        url: 'https://naufal-pokebase.vercel.app/details',
        lastModified: new Date(),
      },
    ]
  }