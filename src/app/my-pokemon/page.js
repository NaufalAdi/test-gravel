import dynamic from 'next/dynamic';
import Head from 'next/head';
const MyPokemonList = dynamic (()=> import('@/components/pokemon-details/myPokemonList'),{ssr:false});

export default function Page() {
    return (
      <>
      <MyPokemonList></MyPokemonList>
      </>
    )
}