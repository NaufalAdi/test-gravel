import dynamic from "next/dynamic"
const AllPokemonList = dynamic (()=> import('@/components/pokemon-list/allPokemonList'),{ssr:false});

export default async function Page() {
  return (
    <>
        <AllPokemonList></AllPokemonList>
    </>

  )
}
