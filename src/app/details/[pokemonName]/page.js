import Image from "next/image"
import { titleCase } from "@/components/functions"
import CatchRelease from "@/components/pokemon-list/catchRelease"

async function getPokemon(name) {
  const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${name}`)
  const data = await res.json()
  return data
}

export default async function Page({params}) {
    const data = await getPokemon(params.pokemonName)
    function heightFormat (height) {
      return `${height * 10} cm`
    }
    function weightFormat (weight) {
      return `${weight / 10} kg`
    }
    return (
    <div className=" bg-white p-6 max-w-4xl mx-auto flex flex-col">
      <h1 className="font-semibold text-3xl text-center">{titleCase(data.name)}</h1>
      <CatchRelease name={data.name} id={data.id} className="self-center  mt-6"/>
      <div className="grid grid-cols-1 sm:grid-cols-12 w-full p-6 gap-3">
        {/* IMAGE */}
        <div className="flex h-full col-span-12 sm:col-span-4 select-none">
          <img className="object-center w-full max-w-[150px] m-auto" alt={`image of ${data.name}`} src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${data.id}.png`}/>
        </div>

        {/* MAIN INFOBOX */}
        <div className="rounded-lg bg-[#30A7D7] flex flex-col col-span-12 sm:col-span-8 gap-2 p-3">
          <div className="grid grid-cols-2 gap-3">
            <p><span className="text-white text-sm">Height</span><br/> <span className="font-semibold">{heightFormat(data.height)}</span></p>
            <p><span className="text-white text-sm">Weight</span><br/> <span className="font-semibold">{weightFormat(data.weight)}</span></p>
            <div>
            <p className="text-white text-sm">Type</p>
            <div className="inline-flex flex-col flex-wrap">
              {data.types.map((obj, idx) => (
                <div 
                className="font-semibold"
                key={idx}>
                  {titleCase(obj.type.name)}
                </div>
              ))}
            </div>
          </div>
          <div>
          <p className="text-white text-sm">Abilities</p>
            <div className="inline-flex flex-col flex-wrap">
              {data.abilities.map((obj, idx) => (
                <div 
                className=" font-semibold" 
                key={idx}>
                  {titleCase(obj.ability.name)}
                </div>
              ))}
            </div>
          </div>
          </div>
        </div>
        {/* STAT INFOBOX */}
        <div className="rounded-lg bg-[#30A7D7] grid grid-cols-3 col-span-12 gap-3 p-3">
        <p><span className="text-white text-sm">Height</span><br/> <span className="font-semibold">{data.stats[0].base_stat}</span></p>
        <p><span className="text-white text-sm">Attack</span><br/> <span className="font-semibold">{data.stats[1].base_stat}</span></p>
        <p><span className="text-white text-sm">Defense</span><br/> <span className="font-semibold">{data.stats[2].base_stat}</span></p>
        <p><span className="text-white text-sm">Special Attack</span><br/> <span className="font-semibold">{data.stats[3].base_stat}</span></p>
        <p><span className="text-white text-sm">Special Defense</span><br/> <span className="font-semibold">{data.stats[4].base_stat}</span></p>
        <p><span className="text-white text-sm">Speed</span><br/> <span className="font-semibold">{data.stats[5].base_stat}</span></p>
        </div>
        {/* MOVES INFOBOX */}
        <div className="bg-red-400 rounded-lg col-span-12 gap-2 p-3">
          <p className="text-sm text-white text-center" >Moves</p>
          <div className="inline-flex flex-wrap justify-center w-full">
            {data.moves.map((obj, idx) => (
              <div 
              className="font-semibold m-1 p-0.5 w-28 rounded text-center" 
              key={idx}>
                {titleCase(obj.move.name)}
              </div>
            ))}
        </div>
      </div>
      </div>
    </div>
    
    )
}