export default function Loading() {
  return (
    <div class="flex h-screen">
      <div class="m-auto">
        <p className="text-xl ">Loading ... </p>
      </div>
    </div>
  )
}