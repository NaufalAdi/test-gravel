export default function Loading() {
    return (
      <div class="flex h-screen">
        <div class="m-auto">
          <p>Loading</p>
        </div>
      </div>
    )
  }