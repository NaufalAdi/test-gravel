'use client'
import Card from "../pokemon-list/card"
import React, { useState } from "react"
import { ReleasePokemon } from "../pokemon-list/catchRelease"

export default function MyPokemonList() {
    const stored = localStorage.getItem("my-pokemon")
    const [pokemonList, setPokemonList] = useState(stored? JSON.parse(stored):[])

    function handleRelease(name) {
        let newList = pokemonList.filter((obj) => obj.name !== name)
        console.log(newList)
        setPokemonList([...newList])
    }

    return (
    <main className='container m-auto p-6 gap-4'>
        <div className='inline-flex flex-row flex-wrap 
        flex-1 gap-6 justify-center w-full'>
        {pokemonList.length > 0 ? pokemonList.map((pokemon, idx) => (
            <div key={idx}>
                <Card data={pokemon}></Card>
                <div onClick={() => handleRelease(pokemon.name)}>
                    <ReleasePokemon className='w-full rounded-none' name={pokemon.name}/>
                </div>
            </div>
        )): <p>You do not own any Pokemon</p>
        }
        </div>
    </main>
    )
}