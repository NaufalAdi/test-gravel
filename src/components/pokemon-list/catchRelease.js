'use client'
import React, { useEffect, useState } from 'react'
import { randInt, titleCase } from '../functions'
import toast from 'react-hot-toast'

export function CatchPokemon({name, id, className, setIsCaught}) {
    function handleCatch(e) {
        e.preventDefault()
        let success = Boolean(randInt(0, 2))
        if (success) {
            toast.success("Successful catch!")
            let stored = localStorage.getItem("my-pokemon")
            
            if(stored){
                let toStore = JSON.parse(stored)
                toStore.push({
                    "name": name,
                    "url": `https://pokeapi.co/api/v2/pokemon/${id}/`
                })
                localStorage.setItem("my-pokemon", JSON.stringify(toStore))
            }
            else{
                let toStore = [{
                    "name": name,
                    "url": `https://pokeapi.co/api/v2/pokemon/${id}/`
                }]
                localStorage.setItem("my-pokemon", JSON.stringify(toStore))
            }
            setIsCaught(true)
        } else{
            toast.error("Try again!")
        }
    }
    return (
        <button onClick={handleCatch} className={`rounded-lg bg-red-400 p-3 text-xl w-32 text-white font-semibold hover:bg-[hsl(11,89%,37%)] ${className}`}>Catch!</button>
        
    )
}

export function ReleasePokemon({name, className, setIsCaught}) {
    function handleRelease(e) {
        e.preventDefault
        let stored = JSON.parse(localStorage.getItem("my-pokemon"));
        stored = stored.filter((obj) => obj.name !== name);
        localStorage.setItem("my-pokemon", JSON.stringify(stored))
        setIsCaught && setIsCaught(false)
        toast.success(`${titleCase(name)} released!`)
    }
    return (
        <button onClick={handleRelease} className={`rounded-lg bg-red-400 p-3 text-xl w-32 text-white font-semibold hover:bg-[hsl(11,89%,37%)] ${className}`}>Release!</button>
    )
}

export default function CatchRelease({name, id, className}) { 
    function checkIsCaught(){
        if (typeof window !== 'undefined') {
            let stored = localStorage.getItem("my-pokemon")
            stored = JSON.parse(stored)
            if (stored && stored.filter(obj => obj.name === name).length > 0){
                return true
            } else {
                return false
            }
          }
    }
    const [isCaught, setIsCaught] = useState(false)

    useEffect(() => {
        setIsCaught(checkIsCaught());
    }, []);
    
    return(
        <>
            {isCaught ? <ReleasePokemon name={name} className={className} setIsCaught={setIsCaught}/>: 
            <CatchPokemon name={name} id={id} className={className} setIsCaught={setIsCaught}/>}
        </>
    )
 }