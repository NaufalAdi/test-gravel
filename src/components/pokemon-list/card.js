'use client'
import Link from "next/link"
import { titleCase } from "../functions"
import Image from "next/image"

export default function Card({data, children, className}) {
  const id = data.url.split("/").at(-2)
  return (
    <Link href={'/details/'+data.name}>
      <div className={`p-6 bg-white w-40 shadow-lg text-center hover:bg-gray-100 ${className}`}>
        <div className="relative h-32 select-none">
          <Image width={120} height={120} className="select-none object-center max-w-[150px] m-auto" alt={`image of ${data.name}`} src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`}></Image>
        </div>
        <p className="text-lg">{titleCase(data.name)}</p>
        {children}
      </div>
    </Link>
  )
}
