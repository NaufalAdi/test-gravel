'use client'
import Card from "./card"
import Pagination from "./pagination"
import React, { useState, useEffect, useRef } from "react"

async function getPokemonList(limit, offset) {
  const res = await fetch(`https://pokeapi.co/api/v2/pokemon/?limit=${limit}&offset=${offset}`)
  const data = await res.json()
  return data
}

export default function AllPokemonList() {
  const [currentPage, setCurrentPage] = useState(1)
  const [limit, setLimit] = useState(12)
  const [pokemonList, setPokemonList] = useState([])
  const [count, setCount] = useState([])
  let offset = (currentPage - 1) * limit

  async function fetchData() {  
    const data =  await getPokemonList(limit, offset)
    setPokemonList(data.results)
    setCount(data.count)
  }

  useEffect(() => {
    fetchData()
  },[currentPage, limit])

  return (
  <main className='container m-auto p-6 gap-4'>
    <div className='inline-flex flex-row flex-wrap 
      flex-1 gap-6 justify-center w-full'>
      {pokemonList.map((pokemon, idx) => (
        <Card key={idx} data={pokemon}></Card>
      ))}
    </div>
    <Pagination
      count={count}
      limit={limit}
      currentPage={currentPage}
      setCurrentPage={setCurrentPage}
      className="justify-center my-6"
    />
  </main>
  )
}