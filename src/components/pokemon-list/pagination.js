'use client'

export default function Pagination({
    count,
    limit,
    currentPage,
    setCurrentPage,
    className
}) {
    const pages = []
    let lastPage = Math.ceil(count / limit);
    for (let i = 1; i <= lastPage; i++) {
        pages.push(i);
    }
    console.log(pages)
  return (
    <div className={`flex flex-wrap gap-1 ${className}`}>
        <button
            className = 
            "p-1 w-10 rounded transition bg-white text-sm font-bold active:bg-red-400 hover:bg-red-400"
            key={1}
            onClick={() => setCurrentPage(1)}>
            {'<<'}
        </button>
        {pages.slice(Math.max(currentPage-2,0), Math.min(currentPage+1,lastPage)).map((page, index) => {
        return (
            <button
                className = 
                {`p-1 min-w-[2.5rem] rounded transition ${page == currentPage ? 'bg-red-400':'bg-white'} text-sm font-bold active:bg-red-400 hover:bg-red-400`}
                key={index}
                onClick={() => setCurrentPage(page)}>
                {page}
            </button>
        )})}
        <button
            className = 
            "p-1 w-10 rounded transition bg-white text-sm font-bold  active:bg-red-400 hover:bg-red-400"
            key={lastPage}
            onClick={() => setCurrentPage(lastPage)}>
            {'>>'}
        </button>
    </div>
  )
}