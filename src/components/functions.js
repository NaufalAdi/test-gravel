export function titleCase(str) {
    str = str.replace(/-/g, " ")
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
    }
    return splitStr.join(' '); 
}

export function randInt(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}