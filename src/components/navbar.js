'use client'
import Link from "next/link"
import { usePathname } from 'next/navigation'

function NavItem({href, className, children}){
  const pathname = usePathname()
  const isActive = pathname == href
  return (
    <Link href={href} className={`align-middle text-center ${isActive ? 'bg-red-400 text-white':'bg-transparent'} ${className}`}>{children}</Link>
  )
}

export default function Navbar() {
    return (
      <nav className="px-6 h-fit mx-auto bg-white shadow-lg flex flex-wrap ">
        <div className="text-xl my-auto font-extrabold w-full text-center sm:absolute sm:left-6 sm:top-2 sm:w-fit sm:my-auto">PokeBase</div>
        <div className="flex justify-center align-middle w-full">
          <NavItem href="/" className='p-3 h-full w-48 font-semibold'>List of Pokemon</NavItem>
          <NavItem href="/my-pokemon" className='p-3 h-full w-48 font-semibold'>My Pokemon</NavItem>
  
        </div>

      </nav>
    )
  }